# ethereum-stack-1

## Start 

Register account at https://moncc.io/join

Get moncc - https://docs.moncc.io/getting-started/moncc-in-10-minutes

Start `monccd -d` and login to Moncc.

```
» mncc login --email=$MONCC_USERNAME --password=$MONCC_PASS
```

## Create a cluster

Create new cluster 
```
» mncc cluster new --name=$CLUSTER_NAME --link=true
```

Prepare cloud credentials:
- GCP service account key `/usr/local/etc/key.json` or other directory with full Compute access role.
```
» mncc cluster provider add -p gcp -f /usr/local/etc/key.json
```
- AWS IAM role with full EC2 access role and save your secret keys under `~/.aws/credentials` - Moncc will automatically source from `[default]`
```
[default]
aws_access_key_id=<aws_access_key_id>
aws_secret_access_key=<aws_secret_access_key>

» mncc cluster provider add -p aws
```

Provision cluster node instances
- $PEER_TAG - tag name for instances in the clusters. It's used to point your deployments to selected tags.
```
» mncc cluster grow --provider=gcp --name=n2-gcp --tag=$PEER_TAG --instance-type=n2-standard-2 --region=europe-west4 --disk-size=10 -m 2
```

Retrieve Monccode for Gitlab to connect to the cluster.
```
» mncc cluster info
✔ Got the list of peers
✔ Got cluster info
Node ID: QmecNCwVApSKXaGeDjmx92ZWPEhyTrZ9RECWMRcFFn9J9g

Token: 9496aeeb-1deb-5237-b07e-d0a15a7962e7

Node addresses: /ip4/...

Monccode: H4sIAAAAAAAA/6zU0XKTQBQG4Bdylj0HCOylCRCHWhA2AcpdA5FiJxRRpHX68I4pOtrkJtP/BT7O2f/wJ4d9Fa2m7H2vr4rb9d77cnhUXOaf/LunzVCq1F/l12kVBJ0KVfMclGGsQ3+bRx/jLNsG+jpOyizdFEv/wzLP42ej7S2DHBZsCynI+F71hmVJSUbPvXHZ1979xUghMQeImRKJERBjF4khA2ALiSHTJOibQdNkJAY7WsWCFq4gQRI120tvjPUf7OvYVojyQIoOWpxrBCkSWpwLBSnCk5mrBSnCsyb8O+KzZriIvfB/iwdkquPOri2IFy+mw2wDzWNBHk0c96a+Pd0YkIwUtPj9F/LpgK2uDrdjpCcv2t+0q8JMdmYWJrWu+/BpSjNvuBrqZqR2Gk6w8+NdLJq2YGkJYhIOYsL/PdCQ89pkC2bzPLrrP4fhg1t36+hn82Nzs+wKbz3d+Y+tXpRd+y1r9P2D2u7s8Z7Ooa83v9wzLaGkIOkIkgruvXnpXwEAAP//UzIr3NkMAAA=
```

Add/Update CICD variable in Gitlab - CI_CLUSTER_MONCCODE to Monccode value above.

### Deploy

Load the template
```
» mncc load stack.yaml 
```

Login to Gitlab container registry
- $GITLAB_USERNAME - your Gitlab username 
- $GITLAB_ACCESS_TOKEN - use existing access token, or create a new one under Account Settings/Access Token.

```
» mncc docker-login -s registry.gitlab.com --username=$GITLAB_USERNAME --password=$GITLAB_ACCESS_TOKEN
✔ Successfully authenticated to registry.gitlab.com
```

Deploy the stack 
```
ethereum-fork(master) » mncc run -t $PEER_TAG defi/stack
✔ Starting the job... DONE
✔ Preparing nodes DONE
✔ Preparing containers DONE
✔ Checking/pulling images DONE

✨ All done! 

🔩 defi/stack
 └─🧊 Peer QmNYXDcHPCKQJGoZM1bamhXeP5VLJ9Lbhp8kbRenHi6Whc
    ├─📦 templates-local-defi-ganache-cli-1-ganache-cli
    │  ├─🧩 registry.gitlab.com/maciej22/ethereum-fork/ganache:master
    │  └─🔌 open 34.91.31.95:8545 (0.0.0.0:8545) -> 8545
    ├─📦 templates-local-defi-poster-1-poster
    │  └─🧩 registry.gitlab.com/maciej22/ethereum-fork/poster:master
    ├─📦 templates-local-defi-client-1-truffle-drizzle
    │  ├─🧩 registry.gitlab.com/maciej22/ethereum-fork/interface:master
    │  └─🔌 open 34.91.114.193:3000 (0.0.0.0:3000) -> 3000
    └─📦 templates-local-defi-reporter-1-reporter
       ├─🧩 registry.gitlab.com/maciej22/ethereum-fork/reporter:master
       └─🔌 open 34.91.114.193:4040 (0.0.0.0:4040) -> 4040

💡 You can inspect and manage your above stack with these commands:
	mncc logs (-f) defi/stack - Inspect logs
	mncc shell     defi/stack - Connect to the container's shell
	mncc do        defi/stack/action_name - Run defined action (if exists)
💡 Check mncc help for more!
```

### Update

Commit and push changes to master.

### Interact

- To test the web app firstly in Metamask create Custom RPC connecting to deployed Ganache network i.e above http://34.91.31.95:8545
- Switch Metamask Ethereum wallet to the below
```
Available Accounts
==================
(0) 0x95eDA452256C1190947f9ba1fD19422f0120858a (100 ETH)

Private Keys
==================
(0) 0x31c354f57fc542eba2c56699286723e94f7bd02a4891a0a7f68566c2a2df6795
```
- Open web app in the browser i.e as abive 34.91.114.193:3000

- To trigger Open Price Feed poster you can execute Moncc Action below
```
mncc do defi/poster-1/post
```
- Track updates with 
```
mncc logs -f defi/ganache-cli-1
```

