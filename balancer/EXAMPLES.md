# Init
Pick one of the accounts to act as an owner
```
Available Accounts
(0) 0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 (100 ETH) <-
(1) 0x95eDA452256C1190947f9ba1fD19422f0120858a (100 ETH)
(2) 0x434BE6240f6dC213EbE695EB55a3f07f6C5148E3 (100 ETH)
```

# Create a new Balancer Pool
```
mncc do defi/balancer-1/create-pool account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5
```
Save pool address for below.

# Create dummy ERC20 tokens to submit to the new Balancer Pool
```
mncc do defi/balancer-1/erc20-deploy --account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 --name="Maker" --symbol="MKR"
mncc do defi/balancer-1/erc20-deploy --account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 --name="Dai" --symbol="Dai"
```
Save ERC20 addresses for below.

# Mint dummy ERC20 tokens to selected accounts
```
mncc do defi/balancer-1/erc20-mint-to-account owner=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 account=0x95eDA452256C1190947f9ba1fD19422f0120858a --amount=1000
```

# Approve tokens into pool
```
mncc do defi/balancer-1/pool-approve-erc20 account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 token=0x055cBfeD6df4AFE2452b18fd3D2592D1795592b4 pool=0x643605E038abd49125Fc80F7dd420dAa161beEA2
```

# Bind tokens into pool
```
mncc do defi/balancer-1/pool-bind-erc20 account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 token=0xb63564A81D5d4004F4f22E9aB074cE25540B0C26 pool=0x78474c7f874b78b7d2585f4AFC76C2fDf9e82C00 balance=10 denorm=10
```

# Finalise new pool
```
mncc do defi/balancer-1/pool-finalise account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 pool=0x78474c7f874b78b7d2585f4AFC76C2fDf9e82C00 fee=0.003
```

# Trades

# Swap in
```
mncc do defi/balancer-1/pool-swap-in account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 pool=0x78474c7f874b78b7d2585f4AFC76C2fDf9e82C00 token_sell=0x434BE6240f6dC213EbE695EB55a3f07f6C5148E3 token_buy=0x434BE6240f6dC213EbE695EB55a3f07f6C5148E3 sell_amount=2 min_amount_out=1 max_price=400
```

# Swap out
```
mncc do defi/balancer-1/pool-swap-out account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 pool=0x78474c7f874b78b7d2585f4AFC76C2fDf9e82C00 token_sell=0x434BE6240f6dC213EbE695EB55a3f07f6C5148E3 token_buy=0x434BE6240f6dC213EbE695EB55a3f07f6C5148E3 buy_amount=2 max_amount_in=1 max_price=400
```