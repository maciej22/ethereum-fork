
# Init
```
mncc do defi/unlocker-1/unlock addr=0xd8dA6BF26964aF9D7eEd9e03E53415D37aA96045
mncc do defi/unlocker-1/mint-dai to=0x95eDA452256C1190947f9ba1fD19422f0120858a amount=2000
```

By default, `0x95eDA452256C1190947f9ba1fD19422f0120858a` is unlocked by ganache.

# Uniswap Trades
## DAI to ETH
```
mncc do defi/tools-1/uni-trade account=0x95eDA452256C1190947f9ba1fD19422f0120858a input=0x6b175474e89094c44da98b954eedeac495271d0f output=ETH amount=1000
```
## ETH to DAI
```
mncc do defi/tools-1/uni-trade account=0x95eDA452256C1190947f9ba1fD19422f0120858a input=ETH output=0x6b175474e89094c44da98b954eedeac495271d0f amount=2
```
## DAI to USDC
```
mncc do defi/tools-1/uni-trade account=0x95eDA452256C1190947f9ba1fD19422f0120858a input=0x6b175474e89094c44da98b954eedeac495271d0f output=0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48 amount=100
```

# Liquidity
## Add DAI and ETH
```
mncc do defi/tools-1/uni-liq account=0x95eDA452256C1190947f9ba1fD19422f0120858a token_a=ETH token_b=0x6b175474e89094c44da98b954eedeac495271d0f amount_a=1 amount_b=500
```
## Remove DAI and ETH
Where **LIQ_AMOUNT** was printed via the previous command (the line mentioning liquidity tokens).
```
mncc do defi/tools-1/uni-liq account=0x95eDA452256C1190947f9ba1fD19422f0120858a token_a=0x6b175474e89094c44da98b954eedeac495271d0f token_b=ETH provide=false liq_amount=LIQ_AMOUNT
```
## Add DAI and USDC
```
mncc do defi/tools-1/uni-liq account=0x95eDA452256C1190947f9ba1fD19422f0120858a token_a=0x6b175474e89094c44da98b954eedeac495271d0f token_b=0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48 amount_a=50 amount_b=50
```

# Compound
> Compound already resolve supported token names so no need to use addresses!
## Supply ETH
```
mncc do defi/tools-1/comp-supply account=0x95eDA452256C1190947f9ba1fD19422f0120858a asset=ETH amount=2
```
## Enable it as a collateral
```
mncc do defi/tools-1/comp-enable account=0x95eDA452256C1190947f9ba1fD19422f0120858a asset=ETH
```
## Borrow some USDC
```
mncc do defi/tools-1/comp-borrow account=0x95eDA452256C1190947f9ba1fD19422f0120858a asset=USDC amount=100
```
## Repay the loan
```
mncc do defi/tools-1/comp-repay account=0x95eDA452256C1190947f9ba1fD19422f0120858a asset=USDC amount=100
```
## Disable ETH collateral
```
mncc do defi/tools-1/comp-disable account=0x95eDA452256C1190947f9ba1fD19422f0120858a asset=ETH
```
## Get some of the ETH back
```
mncc do defi/tools-1/comp-redeem account=0x95eDA452256C1190947f9ba1fD19422f0120858a asset=ETH amount=1
```

# Balancer

# Init
Pick one of the accounts to act as an owner
```
Available Accounts
(0) 0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 (100 ETH) <-
(1) 0x95eDA452256C1190947f9ba1fD19422f0120858a (100 ETH)
(2) 0x434BE6240f6dC213EbE695EB55a3f07f6C5148E3 (100 ETH)
```

# Create a new Balancer Pool
```
mncc do defi/balancer-1/create-pool account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5
```
Save pool address for below.

# Create dummy ERC20 tokens to submit to the new Balancer Pool
```
mncc do defi/balancer-1/erc20-deploy --account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 --name="Maker" --symbol="MKR"
mncc do defi/balancer-1/erc20-deploy --account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 --name="Dai" --symbol="Dai"
```
Save ERC20 addresses for below.

# Mint dummy ERC20 tokens to selected accounts
```
mncc do defi/balancer-1/erc20-mint-to-account owner=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 account=0x95eDA452256C1190947f9ba1fD19422f0120858a --amount=1000
```

# Approve tokens into pool
```
mncc do defi/balancer-1/pool-approve-erc20 account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 token=0x055cBfeD6df4AFE2452b18fd3D2592D1795592b4 pool=0x643605E038abd49125Fc80F7dd420dAa161beEA2
```

# Bind tokens into pool
```
mncc do defi/balancer-1/pool-bind-erc20 account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 token=0xb63564A81D5d4004F4f22E9aB074cE25540B0C26 pool=0x78474c7f874b78b7d2585f4AFC76C2fDf9e82C00 balance=10 denorm=10
```

# Finalise new pool
```
mncc do defi/balancer-1/pool-finalise account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 pool=0x78474c7f874b78b7d2585f4AFC76C2fDf9e82C00 fee=0.003
```

# Trades
# Swap in
```
mncc do defi/balancer-1/pool-swap-in account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 pool=0x78474c7f874b78b7d2585f4AFC76C2fDf9e82C00 token_sell=0x434BE6240f6dC213EbE695EB55a3f07f6C5148E3 token_buy=0x434BE6240f6dC213EbE695EB55a3f07f6C5148E3 sell_amount=2 min_amount_out=1 max_price=400
```
# Swap out
```
mncc do defi/balancer-1/pool-swap-out account=0x08302CF8648A961c607e3e7Bd7B7Ec3230c2A6c5 pool=0x78474c7f874b78b7d2585f4AFC76C2fDf9e82C00 token_sell=0x434BE6240f6dC213EbE695EB55a3f07f6C5148E3 token_buy=0x434BE6240f6dC213EbE695EB55a3f07f6C5148E3 buy_amount=2 max_amount_in=1 max_price=400
```
